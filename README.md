## **RoHGIS. Date istorico-geografice**

Depozit, alcătuit inițial pe 10 ianuarie 2017, ce conține diverse date istorico-geografice, în completarea colecțiilor eHarta și RoHGIS. Datele vor putea fi obținute prin git sau descărcate în formate interoperabile, ușor de prelucrat, în codificare UTF-8:

- `csv` (cu separator tab) sau `xml` pentru date non-geografice
- `gpkg` pentru date geografice în proiecție Stereografică 1970 (EPSG:3844)

Colecția va fi actualizată periodic.

Datele pot fi folosite liber, în scopuri științifice sau educaționale, cu precizarea sursei. Pentru semnalări de greșeli, probleme, întrebări sau observații, vă rugăm să lăsați un mesaj.

### Date geografice

| set de date | sursa | ultima actualizare | descărcare |
| ----------- | ----- | ------------------ | ---------- |
| Județele și plășile Regatului României din 1930 | RoHGIS 2016 | 02.06.2020 | [gpkg](/date_geografice/date_rohgis_v01.gpkg) | 


### Date statistice

| set de date | sursa | ultima actualizare | descărcare |
| ----------- | ----- | ------------------ | ---------- |
| statistici etnice pentru județele Regatului României în 1930 | Recensămîntul general al populației României, 1930 | 12.12.2022 | [csv](/date_statistice/rohgis_statistici_etnice_1930.csv), [txt cu explicații](/date_statistice/rohgis_statistici_etnice_1930_legenda.txt) |
